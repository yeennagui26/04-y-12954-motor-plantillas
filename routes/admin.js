const express = require ("express");
const router = express.Router();

router.get("/", (req, res)=> {
    res.render("admin/index");
});

//como prefijo admin
router.get("/integrantes/listar", (req, res)=> {
    res.render("admin/integrantes/index",{
        integrantes: [],
    });
});

module.exports = router;

    