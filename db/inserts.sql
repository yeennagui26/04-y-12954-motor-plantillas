-- INSERT INTO la tabla de Integrantes
--INSERT INTO Integrantes (matricula, nombre, apellido, url, orden) VALUES
--('Y26230', 'Zuanny', 'Ortiz', 'Y26230', 1),
--('Y23715', 'Gabriela', 'Espinola', 'Y23715', 2),
--('UG0045', 'Diego', 'Ramirez', 'UG0045',3),
--('Y12954', 'Yennifer', 'Aguilera', 'Y12954',4),
--('Y18433', 'Nicolas', 'Gimenez', 'Y18433',5);

-- INSERT INTO la tabla de TipoMedia
--INSERT INTO TipoMedia (tipo_media, nombre, orden) VALUES
--('Imagen',1),
--('Youtube',2),
--('Dibujo',3);

-- INSERT INTO la tabla de Media
--nicolas media
-- INSERT INTO Media
INSERT INTO Media (titulo, nombre, url, matricula, id_tipo_media, orden) VALUES 
('video Youtube', 'Youtube', 'https://www.youtube.com/embed/YZ8j6iO0ulw?si=3qmb7GQ0CxHN2C3Q', 'Y18433', 2, 1),
('imagen elegida', 'imagen', '/images/mis_sueños.jpeg', 'Y18433', 1, 2),
('dibujo hecho', 'dibujo', '/images/DibujoNico.png', 'Y18433', 3, 3),

('video Youtube', 'Youtube', 'https://www.youtube.com/embed/210R0ozmLwg?si=bbjXLbzQS8fBaoTz', 'UG0045', 2, 4),
('imagen elegida', 'imagen', '/images/imagen1Diego.jpeg', 'UG0045', 1, 5),
('dibujo hecho', 'dibujo', '/images/Imagen2Diego.jpeg', 'UG0045', 3, 6),

('video Youtube', 'Youtube', 'https://www.youtube.com/embed/yKNxeF4KMsY?si=_ipSNyBEBlw03PPN', 'Y12954', 2, 7),
('imagen elegida', 'imagen', '/images/yeniimagen.jpg', 'Y12954', 1, 8),
('dibujo hecho', 'dibujo', '/images/yenidibujo.jpg', 'Y12954', 3, 9),

('video Youtube', 'Youtube', 'https://www.youtube.com/embed/Rk7gnFCeVAY', 'Y23715', 2, 10),
('imagen elegida', 'imagen', '/images/Bob Esponja.jpeg', 'Y23715', 1, 11),
('dibujo hecho', 'dibujo', '/images/bananamichi.png', 'Y23715', 3, 12),

('video Youtube', 'Youtube', 'https://www.youtube.com/embed/Qes1RMK9a50?si=U5YTt8otRVQc1Ahk', 'Y26230', 2, 13),
('imagen elegida', 'imagen', '/images/Extrovertida.jpg', 'Y26230', 1, 14),
('dibujo hecho', 'dibujo', '/images/Dibujo_Zuanny.png', 'Y26230', 3, 15);



